from django.http import HttpResponse
from django.shortcuts import render


def home(request):
    return render(request, 'index.html')


def analyze(request):
    # get the text
    text = request.GET.get('text', 'default')
    # check checkbox values
    removefcn = request.GET.get('removefcn', 'Off')
    fullcaps = request.GET.get('fullcaps', 'off')
    newlineremover = request.GET.get('newlineremover', 'off')
    extraspaceremover = request.GET.get('extraspaceremover', 'off')
    charcounter = request.GET.get("charcounter", 'off')
    # check which checkbox is on
    if removefcn == 'on':
        punctuation = '''!()-[]{};:'"\,<>.`/?@#$%^&*_~'''
        analyzed = ""
        for char in text:
            if char not in punctuation:
                analyzed = analyzed + char
        context = {'purpose': 'Remove Punctuation', 'analyzed_text': analyzed}
        return render(request, 'analyze.html', context)
    elif fullcaps == "on":
        analyzed = ""
        for char in text:
            analyzed = analyzed + char.upper()
        context = {'purpose': 'Changed to Uppercase', 'analyzed_text': analyzed}
        return render(request, 'analyze.html', context)
    elif newlineremover == "on":
        analyzed = ""
        for char in text:
            if char != "\n":
                analyzed = analyzed + char
        context = {'purpose': 'Remove New Lines', 'analyzed_text': analyzed}
        return render(request, 'analyze.html', context)
    elif extraspaceremover == "on":
        analyzed = ""
        for index, char in enumerate(text):
            if text[index] == " " and text[index + 1] == " ":
                pass
            else:
                analyzed = analyzed + char
        context = {'purpose': 'Remove Extra Spaces', 'analyzed_text': analyzed}
        return render(request, 'analyze.html', context)
    elif charcounter == "on":
        analyzed = ""
        for char in analyzed:
            countvalue = analyzed.count(char)
            count = (char, "appears", countvalue, "times")
        context = {'purpose': 'Count the Character', 'analyzed_text': count}
        return render(request, 'analyze.html', context)
    else:
        return HttpResponse("Please Click Remove Punctuations Button")
